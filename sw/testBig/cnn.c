/*
 * Copyright 2016 Andrea Solazzo, Emanuele Del Sozzo, Gianluca Durelli, Matteo De Silvestri, Irene De Rose
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * 	http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "parameters.h"

#define REAL float
#define BIT_WIDTH 32

/* DMA constants */
#define WINDOW FM_0*DIMW_0*KH_1
#define NEXT_ROW FM_0*DIMW_0*(KH_1-1)

int cnn(REAL input[FM_0*DIMH_0*DIMW_0])
{
	int i, j, k, l, m, n, s, t, z;
	REAL x, v, y, max = -HUGE_VAL;
	REAL predicted;

//DMA buffer and pointers
	REAL buf[WINDOW];
	int indexes[FM_0 * KH_1];
	int actual_index;

//Reads first elements
	for(i = 0; i < WINDOW; i++){
		buf[i] = input[i];
	}

	//Keep track of number of read bytes
	int input_index = i;

	//Load rows indexes
	for(i = 0, j = 0; i < KH_1; i++, j+=DIMW_0){
		indexes[i] = j;
	}
//1 layer (convolutive):
//Convolution

	//Load biases
	for (k = 0; k < FM_1; k++) {
		for (i = 0; i < DIMH_1; i++) {
			for (j = 0; j < DIMW_1; j++) {
				o1[k][i][j] = b1[k];
			}
		}
	}

 Conv1:	for (i = 0; i < DIMH_1; i++) {
			for (j = 0; j < DIMW_1; j++) {
				for (l = 0; l < FM_0; l++) {
					for (s = 0; s < KH_1; s++) {
						m = i + s;
						for (t = 0; t < KW_1; t++) {
							n = j + t;
							for (k = 0; k < FM_1; k++) {
							actual_index = indexes[s];
								actual_index += n;
								v = w1[k][l][s][t] *
								    buf[actual_index];
								o1[k][i][j] += v;
							}
						}
					}
				}
			}

			if(i<DIMH_1 - 1){
				//Flush firt row
				for(z = DIMW_0; z < WINDOW; z++){
					actual_index = z-DIMW_0;
					buf[actual_index] = buf[z];
				}
				//Load new row
				for(z = NEXT_ROW; z < WINDOW; z++){
					buf[z] = input[input_index];
					input_index++;
				}
			}
		}

//Pooling
 Pool1:for (k = 0; k < FM_1; k++) {
		for (i = 0; i < PDIMH_1; i++) {
			for (j = 0; j < PDIMW_1; j++) {
				max = -HUGE_VAL;
				for (s = 0; s < PH_1; s++) {
					m = i * PS_1;
					m += s;
					for (t = 0; t < PW_1; t++) {
						n = j * PS_1;
						n += t;
						if (o1[k][m][n] > max) {
							max = o1[k][m][n];
						}
					}
				}
				p1[k][i][j] = max;
			}
		}
	}
//2 layer (convolutive):
//Convolution

//Load biases
	for (k = 0; k < FM_2; k++) {
		for (i = 0; i < DIMH_2; i++) {
			for (j = 0; j < DIMW_2; j++) {
				o2[k][i][j] = b2[k];
			}
		}
	}
 Conv2:for (i = 0; i < DIMH_2; i++) {
		for (j = 0; j < DIMW_2; j++) {
			for (l = 0; l < FM_1; l++) {
				for (s = 0; s < KH_2; s++) {
					m = i + s;
					for (t = 0; t < KW_2; t++) {
						n = j + t;
 conv2_inn:					for (k = 0; k < FM_2;
						     k++) {
							v = w2[k][l][s][t] *
							    p1[l][m][n];
							o2[k][i][j] += v;
						}
					}
				}
			}
		}
	}
//End of convolutional part
//Reshape:
 Reshape:for (k = 0; k < FM_2; k++) {
		for (i = 0; i < DIMH_2; i++) {
			for (j = 0; j < DIMW_2; j++) {
				m = k * DIMH_2;
				m = m * DIMW_2;
				n = i * DIMH_2;
				n += j;
				n += m;
				lin[n] = o2[k][i][j];
			}
		}
	}
//3 layer (linear):
 Lin1:	for (i = 0; i < LIN_1; i++) {
		l1[i] = lb1[i];
		for (j = 0; j < LIN_0; j++) {
			v = lin[j] * lw1[i][j];
			l1[i] += v;
		}
	}
//Classification (LogSoftMax):
	max = -HUGE_VAL;
 Class1:for (i = 0; i < CLASSES; i++) {
		if (l1[i] > max) {
			max = l1[i];
		}
	}

	x = 0;
 Class2:for (i = 0; i < CLASSES; i++) {
		v = l1[i] - max;
		l1[i] = exp(v);
		x += l1[i];
	}

 Class3:for (i = 0; i < CLASSES; i++) {
		res[i] = l1[i] / x;
		res[i] = log(res[i]);
	}

//Prediction:
	max = -HUGE_VAL;
	predicted = -1;
 Pred:	for (i = 0; i < CLASSES; i++) {
		if (res[i] > max) {
			max = res[i];
			predicted = i;
		}
	}

	return predicted;
}
